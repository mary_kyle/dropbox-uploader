package blog.artificialmind.dropbox;

public class DropboxExceptionDisallowedName extends DropboxException {
    public DropboxExceptionDisallowedName(Exception e) {
        super(e);
    }
}
