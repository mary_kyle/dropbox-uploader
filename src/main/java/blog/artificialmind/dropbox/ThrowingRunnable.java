package blog.artificialmind.dropbox;

@FunctionalInterface
public interface ThrowingRunnable {
    void run() throws Exception;
}
