package blog.artificialmind.dropbox;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;
import picocli.CommandLine;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@CommandLine.Command(name = "dropbox-uploader", mixinStandardHelpOptions = true, version = "dropbox-uploader 0.1a",
    description = "Uploads a local folder to Dropbox as fast as possible.")
public class DropboxCli implements Callable<Integer> {

    @CommandLine.Option(
        names = {"--access-token"},
        description = "The Dropbox app key to use.",
        required = true)
    String accessToken;

    @CommandLine.Option(
        names = {"--expected-email"},
        description = "The Dropbox account email address. This is a precaution to make sure you are uploading to the right account.",
        required = true)
    String expectedEmail;

    @CommandLine.Option(
        names = {"--local-folder"},
        description = "The folder to upload. The name of the folder will be renamed into what you specify as remote folder.",
        required = true)
    Path localFolder;

    @CommandLine.Option(
        names = {"--remote-folder"},
        description = "The relative path in Dropbox to upload to. An empty string will dump the content of the local folder into the Dropbox root.",
        required = true)
    String remoteFolder;

    @CommandLine.Option(
        names = {"--parallelism"},
        description = "The number of parallel requests to Dropbox. The faster your connection, the higher this can be. 100 KB/s/thread is a good bandwidth to aim for.",
        required = true)
    int parallelism;

    private DbxClientV2 client;

    public Integer call() throws Exception {
        this.remoteFolder = getRemotePath(localFolder);
        this.client = new DbxClientV2(
            DbxRequestConfig.newBuilder(getClass().getCanonicalName()).build(),
            accessToken
        );

        verifyAccount(client);

        if (!Files.isDirectory(localFolder)) {
            throw new IllegalArgumentException("The given local folder [" + localFolder + "] is not a folder or does not exist.");
        }

        logInfo("Scanning local folder for files to upload...");

        var localDirContent = Files.walk(localFolder).collect(Collectors.toUnmodifiableList());
        var localFolders = localDirContent.stream().filter(Files::isDirectory).collect(Collectors.toUnmodifiableList());
        var localFiles = localDirContent.stream().filter(Files::isRegularFile).collect(Collectors.toUnmodifiableList());

        logInfo("Found [" + localFiles.size() + "] files and [" + localFolders.size() + "] folders.");
        logInfo("Preparing progress information...");

        var progressMonitor = new ProgressMonitor(localFiles, localFolders);

        logInfo("Scanning remote folder [" + remoteFolder + "] for existing files...");

        var deduplicator = new DropboxDeduplicator(client, remoteFolder);

        logInfo("Creating [" + localFolders.size() + "] folders in Dropbox...");

        Flux.fromIterable(localFolders)
            .flatMap(localPath -> Mono
                    .fromRunnable(() -> createRemoteFolder(localPath, deduplicator, progressMonitor))
                    .subscribeOn(Schedulers.elastic())
                    .onErrorResume(DropboxException.class::isInstance, e -> logException(e, "Failed to upload [" + localPath + "]")),
                parallelism
            )
            .blockLast();

        Flux.fromIterable(localFiles)
            .flatMap(localPath -> Mono
                    .fromRunnable(() -> uploadFile(localPath, deduplicator, progressMonitor))
                    .subscribeOn(Schedulers.elastic())
                    .onErrorResume(DropboxException.class::isInstance, e -> logException(e, "Failed to upload [" + localPath + "]")),
                parallelism
            )
            .blockLast();

        return 0;
    }

    private Mono<Void> logException(Throwable throwable, String message) {
        System.err.println(message + ": " + throwable.getMessage());
        return Mono.empty();
    }

    private void uploadFile(Path localPath, DropboxDeduplicator deduplicator, ProgressMonitor progressMonitor) {
        var remotePath = getRemotePath(localPath);

        if (!deduplicator.alreadyExists(remotePath)) {
            new DropboxChunkUploader(client, localPath, remotePath, progressMonitor).upload();
        }

        progressMonitor.markFileAsUploaded(localPath);
    }

    private void createRemoteFolder(Path localPath, DropboxDeduplicator deduplicator, ProgressMonitor progressMonitor) {
        var remotePath = getRemotePath(localPath);

        if (!deduplicator.alreadyExists(remotePath)) {
            DropboxRetry.retry(
                () -> {
                    try {
                        client.files().createFolderV2(remotePath);
                    } catch (DbxException e) {
                        if (e.getMessage() != null && e.getMessage().contains("\"conflict\":\"folder\"")) {
                            // folder already exists...
                        } else {
                            throw new RuntimeException(e);
                        }
                    }
                },
                "Repeatedly failed to create folder [" + remotePath + "]."
            );
        }

        progressMonitor.markFolderAsUploaded(localPath);
    }

    private String getRemotePath(Path localPath) {
        if (!localPath.startsWith(localFolder)) {
            throw new IllegalStateException("Local path [" + localPath + "] is not contained inside the source folder [" + localFolder + "].");
        }

        var relativeLocalPath = localFolder.relativize(localPath);
        var remotePath = String.join("/", remoteFolder, relativeLocalPath.toString()).replace('\\', '/');
        if (!remotePath.startsWith("/")) {
            remotePath = "/" + remotePath;
        }
        if (remotePath.endsWith("/")) {
            remotePath = remotePath.substring(0, remotePath.length() - 1);
        }
        return remotePath;
    }

    private void verifyAccount(DbxClientV2 client) throws DbxException {
        FullAccount account = client.users().getCurrentAccount();
        if (!account.getEmail().equalsIgnoreCase(expectedEmail)) {
            throw new IllegalArgumentException(
                String.format(
                    "The account [%s] belonging to the access token does not match the expected email address [%s].",
                    account.getEmail(),
                    expectedEmail
                )
            );
        }

        logInfo("Connected with account: %s", account);
    }

    private void logInfo(String message, Object... args) {
        System.out.println(String.format(message, args));
    }

    public static void main(String[] args) {
        int exitCode = new CommandLine(new DropboxCli()).execute(args);
        System.exit(exitCode);
    }
}