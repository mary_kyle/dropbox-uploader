package blog.artificialmind.dropbox;

public class DropboxException extends RuntimeException {
    public DropboxException(Exception e) {
        super(e.getMessage(), e);
    }
}
