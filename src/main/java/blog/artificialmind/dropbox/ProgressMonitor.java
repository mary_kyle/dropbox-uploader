package blog.artificialmind.dropbox;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProgressMonitor {
    private final long totalFileSize;
    private final long totalFileCount;
    private final long totalFolderCount;
    private final Map<Path, Long> localFiles;
    private final Set<Path> localFolders;
    private final List<Map.Entry<Instant, Long>> byteHistory = new ArrayList<>();
    private long fileSizeUploaded = 0;

    public ProgressMonitor(List<Path> localFiles, List<Path> localFolders) {
        this.localFiles = localFiles.stream().collect(Collectors.toMap(
            Function.identity(),
            this::getFileSize
        ));
        this.localFolders = new HashSet<>(localFolders);
        this.totalFileSize = this.localFiles.values().stream().mapToLong(x -> x).sum();
        this.totalFileCount = this.localFiles.size();
        this.totalFolderCount = this.localFolders.size();
    }

    private synchronized long getFileSize(Path localFile) {
        try {
            return Files.size(localFile);
        } catch (IOException e) {
            throw new IllegalStateException("Could not determine size of file [" + localFile + "]: " + e.getMessage(), e);
        }
    }

    public synchronized void markFileAsUploaded(Path localFile) {
        Long size = localFiles.remove(localFile);
        if (size != null) {
            fileSizeUploaded += size;
        }

        System.out.println(getFileProgressPrefix() + "Uploaded file [" + localFile + "].");
    }

    private synchronized String getFileProgressPrefix() {
        double percentSizeUploaded = fileSizeUploaded / (double) Math.max(1, totalFileSize) * 100.0;
        double percentCountUploaded = (totalFileCount - localFiles.size()) / (double) Math.max(1, totalFileCount) * 100.0;
        long lastMinuteSize = byteHistory.stream().mapToLong(Map.Entry::getValue).sum();
        var timeLeft = Duration.ofMinutes(1).multipliedBy((totalFileSize - fileSizeUploaded) / Math.max(1, lastMinuteSize));
        return String.format("[Bytes: %5.2f%%, Items: %5.2f%%, Time: %s] ", percentSizeUploaded, percentCountUploaded, timeLeft);
    }

    public synchronized void markFolderAsUploaded(Path localPath) {
        localFolders.remove(localPath);

        System.out.println(getFolderProgressPrefix() + "Created folder [" + localPath + "].");
    }

    private synchronized String getFolderProgressPrefix() {
        double percentCountUploaded = (totalFolderCount - localFolders.size()) / (double) Math.max(1, totalFolderCount) * 100.0;
        return String.format("[%5.2f%%] ", percentCountUploaded);
    }

    public synchronized void uploadedBytes(long count) {
        byteHistory.add(Map.entry(Instant.now(), count));

        var lastMinute = Instant.now().minus(Duration.ofMinutes(1));
        var newByteHistory = byteHistory.stream().filter(e -> e.getKey().compareTo(lastMinute) >= 0).collect(Collectors.toList());

        byteHistory.clear();
        byteHistory.addAll(newByteHistory);
    }
}
