package blog.artificialmind.dropbox;

import com.github.rholder.retry.*;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class DropboxRetry {
    public static <T> T retry(Callable<T> action, String exceptionMessage) {
        Retryer<T> retryer = RetryerBuilder.<T>newBuilder()
            .retryIfException(throwable -> !(throwable instanceof DropboxException))
            .withWaitStrategy(WaitStrategies.exponentialWait(100, 1, TimeUnit.MINUTES))
            .withStopStrategy(StopStrategies.neverStop())
            .withRetryListener(new RetryListener() {
                @Override
                public <V> void onRetry(Attempt<V> attempt) {
                    if (attempt.getAttemptNumber() > 5) {
                        System.err.println(exceptionMessage + " {Retry attempt " + attempt.getAttemptNumber() + "}");
                    }
                }
            })
            .build();
        try {
            return retryer.call(() -> {
                try {
                    return action.call();
                } catch (Exception e) {
                    if (e.getMessage() != null) {
                        if (e.getMessage().contains("\"reason\":\"disallowed_name\"")) {
                            throw new DropboxExceptionDisallowedName(e);
                        }
                    }

                    throw e;
                }
            });
        } catch (ExecutionException e) {
            if (e.getCause() instanceof DropboxException) {
                throw (DropboxException)e.getCause();
            }

            throw new RuntimeException(exceptionMessage + ": " + e.getMessage(), e);
        } catch (Exception e) {
            throw new RuntimeException(exceptionMessage + ": " + e.getMessage(), e);
        }
    }

    public static void retry(ThrowingRunnable action, String exceptionMessage) {
        retry(() -> {
            action.run();
            return null;
        }, exceptionMessage);
    }
}
