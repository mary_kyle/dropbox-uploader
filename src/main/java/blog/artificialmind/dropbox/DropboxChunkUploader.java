package blog.artificialmind.dropbox;

import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.CommitInfo;
import com.dropbox.core.v2.files.UploadSessionCursor;
import com.dropbox.core.v2.files.WriteMode;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

import static java.util.Objects.requireNonNull;

public class DropboxChunkUploader {
    private static final int CHUNK_SIZE = 10_000_000;

    private final DbxClientV2 client;
    private final Path localFile;
    private final String remotePath;
    private final long totalSize;
    private final ProgressMonitor progressMonitor;

    public DropboxChunkUploader(DbxClientV2 client, Path localFile, String remotePath, ProgressMonitor progressMonitor) {
        this.client = requireNonNull(client);
        this.localFile = requireNonNull(localFile);
        this.remotePath = requireNonNull(remotePath);
        this.progressMonitor = requireNonNull(progressMonitor);
        this.totalSize = getFileSize(localFile);
    }

    private long getFileSize(Path localFile) {
        try {
            return Files.size(localFile);
        } catch (IOException e) {
            throw new IllegalStateException("Could not determine size of file [" + localFile + "]: " + e.getMessage(), e);
        }
    }

    private String openSession(DbxClientV2 client) {
        return DropboxRetry.retry(
            () -> client.files().uploadSessionStart(false).uploadAndFinish(emptyInputStream()).getSessionId(),
            "Repeatedly failed to open session for file [" + localFile + "]."
        );
    }

    private ByteArrayInputStream emptyInputStream() {
        return new ByteArrayInputStream(new byte[0]);
    }

    public void upload() {
        if (totalSize <= CHUNK_SIZE) {
            uploadSmallFile();
        } else {
            uploadLargeFile(totalSize);
        }
    }

    private void uploadSmallFile() {
        DropboxRetry.retry(
            () -> {
                try (var localStream = Files.newInputStream(localFile)) {
                    client.files().uploadBuilder(remotePath)
                        .withMode(WriteMode.ADD)
                        .withClientModified(Date.from(Files.getLastModifiedTime(localFile).toInstant()))
                        .uploadAndFinish(localStream);

                    progressMonitor.uploadedBytes(getFileSize(localFile));
                }
            },
            "Repeatedly failed to upload file [" + localFile + "]."
        );
    }

    private void uploadLargeFile(long totalSize) {
        var currentOffset = 0L;
        var sessionId = openSession(client);

        while (currentOffset < totalSize) {
            currentOffset = uploadChunk(sessionId, currentOffset, totalSize);
        }

        closeSession(sessionId, currentOffset);
    }

    private long uploadChunk(String sessionId, final long currentOffset, final long totalFileSize) {
        return DropboxRetry.retry(
            () -> {
                try (var localStream = Files.newInputStream(localFile)) {
                    System.out.println("Uploading chunk [" + currentOffset + "/" + totalFileSize + "] of file [" + localFile + "]...");

                    localStream.skip(currentOffset);
                    var chunkBytes = localStream.readNBytes(CHUNK_SIZE);

                    client.files()
                        .uploadSessionAppendV2(getCursor(sessionId, currentOffset))
                        .uploadAndFinish(new ByteArrayInputStream(chunkBytes), CHUNK_SIZE);

                    progressMonitor.uploadedBytes(chunkBytes.length);

                    return currentOffset + chunkBytes.length;
                }
            },
            "Repeatedly failed to upload chunk [" + currentOffset + "] for file [" + localFile + "]."
        );
    }

    private void closeSession(String sessionId, long currentOffset) {
        DropboxRetry.retry(
            () -> {
                var commitInfo = CommitInfo.newBuilder(remotePath)
                    .withMode(WriteMode.ADD)
                    .withAutorename(false)
                    .withMute(true)
                    .withClientModified(Date.from(Files.getLastModifiedTime(localFile).toInstant()))
                    .build();

                client.files()
                    .uploadSessionFinish(getCursor(sessionId, currentOffset), commitInfo)
                    .uploadAndFinish(emptyInputStream(), 0);
            },
            "Repeatedly failed to close session [" + sessionId + "] for file [" + localFile + "]."
        );
    }

    private UploadSessionCursor getCursor(String sessionId, long currentOffset) {
        return new UploadSessionCursor(sessionId, currentOffset);
    }
}
