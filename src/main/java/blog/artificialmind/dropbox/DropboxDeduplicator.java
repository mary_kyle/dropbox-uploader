package blog.artificialmind.dropbox;

import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class DropboxDeduplicator {
    private final Set<String> allRemoteFiles = new HashSet<>();

    public DropboxDeduplicator(DbxClientV2 client, String remotePath) {
        listExistingFiles(client, remotePath);
    }

    private void listExistingFiles(DbxClientV2 client, String remotePath) {
        AtomicReference<ListFolderResult> listerResult = new AtomicReference<>();

        listerResult.set(
            DropboxRetry.retry(
                () -> client.files().listFolderBuilder(remotePath)
                    .withRecursive(true)
                    .start(),
                "Repeatedly failed to list files at [" + remotePath + "]."
            )
        );

        processListerResult(listerResult.get());

        while (listerResult.get().getHasMore()) {
            listerResult.set(
                DropboxRetry.retry(
                    () -> client.files().listFolderContinue(listerResult.get().getCursor()),
                    "Repeatedly failed to continue listing at [" + remotePath + "]."
                )
            );

            processListerResult(listerResult.get());
        }
    }

    private void processListerResult(ListFolderResult listerResult) {
        listerResult.getEntries().stream()
            .map(Metadata::getPathLower)
            .forEach(allRemoteFiles::add);

        System.out.println("Found [" + listerResult.getEntries().size() + "] remote folders and files.");
    }

    public boolean alreadyExists(String remotePath) {
        return allRemoteFiles.contains(remotePath.toLowerCase());
    }
}
